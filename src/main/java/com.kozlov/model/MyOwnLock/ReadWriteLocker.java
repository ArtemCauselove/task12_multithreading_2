package com.kozlov.model.MyOwnLock;

public class ReadWriteLocker {
    boolean isLocked = false;
    int lockCounter = 0;
    public WriterReaderLock readLock(){
        return new WriterReaderLock(true);
    }
    public WriterReaderLock writeLock(){
        return new WriterReaderLock(false);
    }
}
