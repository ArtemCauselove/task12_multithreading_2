package com.kozlov.model.MyOwnLock;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class WriterReaderLock extends ReadWriteLocker implements Lock  {
    private boolean isFatherWriter;
    private boolean isWriter ;
    private Thread fatherThread;
    WriterReaderLock(Boolean isWriter){
    this.isWriter = isWriter;
        }
    @Override
    public synchronized void lock() {
        Thread currentThread = Thread.currentThread();
        if(isLocked && currentThread!= fatherThread && isFatherWriter) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            isLocked = true;
            lockCounter++;
            isFatherWriter = isWriter;
            fatherThread = currentThread;
        }
    }

    @Override
    public void lockInterruptibly() throws InterruptedException {
    }

    @Override
    public boolean tryLock() {
        return false;
    }

    @Override
    public boolean tryLock(long time, TimeUnit unit) throws InterruptedException {
        return false;
    }

    @Override
    public synchronized void unlock() {
        if(Thread.currentThread() == fatherThread) {
            lockCounter--;
            if (lockCounter == 0) {
                isFatherWriter = false;
                isLocked = false;
                notify();
            }
        }
    }

    @Override
    public Condition newCondition() {
        return null;
    }
}
