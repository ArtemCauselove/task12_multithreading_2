package com.kozlov.model;

import java.util.concurrent.locks.ReentrantLock;

public class SyncronizingExample extends Thread{
    private static Object sync = new Object();
   /* private static Object sync1 = new Object();
    private static Object sync2 = new Object();*/
   private final static ReentrantLock lock = new ReentrantLock();
    private static int counter = 0;
    private void firstMethod(){
        lock.lock();
            System.out.println(++counter);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            finally{
                lock.unlock();
            }
        }
    private void secondMethod(){
        lock.lock();
                System.out.println(++counter);
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                finally{
                    lock.unlock();
                }
    }
    private void thirdMethod(){
        lock.lock();
                    System.out.println(++counter);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    finally{
                        lock.unlock();
                    }

    }
    @Override
    public void run(){
        firstMethod();
        secondMethod();
        thirdMethod();
    }
    public void  begin() {
        SyncronizingExample ex = new SyncronizingExample();
/*        SyncronizingExample ex1 = new SyncronizingExample();
        SyncronizingExample ex2 = new SyncronizingExample();*/
        Thread t1 = new Thread(ex);
        Thread t2 = new Thread(ex);
        Thread t3 = new Thread(ex);
        t1.start();
        t2.start();
        t3.start();
        try {
            t1.join();
            t2.join();
            t3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
