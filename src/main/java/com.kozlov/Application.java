package com.kozlov;

import com.kozlov.View.MyView;

public class Application {
    public static void main(String[] args) {
        MyView view = new MyView();
        view.show();
    }
}
